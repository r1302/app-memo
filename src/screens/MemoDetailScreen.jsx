import React from 'react';
import {
  View, Text, ScrollView, StyleSheet,
} from 'react-native';

import CircleButton from '../components/CircleButton';

// eslint-disable-next-line react/function-component-definition
export default function MemoListScreen(props) {
  const { navigation } = props;
  return (
    <View style={styles.container}>

      <View style={styles.memoHeader}>
        <Text style={styles.memoTitle}>買い物リスト</Text>
        <Text style={styles.memoDate}>2021年12月1日</Text>
      </View>

      <ScrollView style={styles.memoBody}>
        <Text style={styles.memoText}>
          買い物リスト
          ほげhごえhゲオhgへおgふぉgふぉへwじょfんwそfんうぇ
        </Text>
      </ScrollView>

      <CircleButton
        style={{ top: 60, bottm: 'auto' }}
        name="edit-2"
        onPress={() => { navigation.navigate('MemoEditScreen'); }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  memoHeader: {
    backgroundColor: '#C51162',
    height: 100,
    justifyContent: 'center',
    paddingVertical: 24,
    paddingHorizontal: 19,
  },
  memoTitle: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    lineHeight: 32,
  },
  memoDate: {
    color: 'rgba(255,255,255, 0.75)',
    fontSize: 16,
    lineHeight: 24,
  },
  memoBody: {
    paddingVertical: 35,
    paddingHorizontal: 28,
  },
  memoText: {
    fontSize: 16,
    lineHeight: 24,
  },
});
