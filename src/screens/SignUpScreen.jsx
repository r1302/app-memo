import React, { useState } from 'react';
import {
  View, Text, TextInput, StyleSheet, TouchableOpacity, Alert,
} from 'react-native';
import firebase from 'firebase';

import Button from '../components/Button';

// eslint-disable-next-line react/function-component-definition
export default function SignUpScreen(props) {
  const { navigation } = props;
  const [email, setEmail] = useState('');
  const [pass, setPass] = useState('');

  function handleSignup() {
    firebase.auth().createUserWithEmailAndPassword(email, pass)
      .then((userCredential) => {
        const { user } = userCredential;
        console.log(user.uid);
        navigation.reset({
          index: 0,
          routes: [{ name: 'MemoListScreen' }],
        });
      })
      .catch((error) => {
        Alert.alert(error.code);
      });
  }

  return (
    <View style={styles.container}>

      <View style={styles.inputContainer}>
        <Text style={styles.inputTitle}>Sign Up</Text>
        <TextInput
          value={email}
          style={styles.input}
          onChangeText={(text) => { setEmail(text); }}
          /* 入力の最初の文字を大文字にしない */
          autoCapitalize="none"
          /* キーボードのタイプをemailにする */
          keyboardType="email-address"
          placeholder="Email"
          /* iOSでキーチェーンと紐づけてくれる */
          textContentType="emailAddress"
        />
        <TextInput
          value={pass}
          style={styles.input}
          onChangeText={(text) => { setPass(text); }}
          placeholder="Password"
          /* 入力された文字を隠す */
          secureTextEntry
          /* iOSでキーチェーンと紐づけてくれる */
          textContentType="password"
        />

        <Button
          onPress={handleSignup}
        >
          Sign Up
        </Button>

        <View>
          <Text style={styles.footerText}>Already resistered?</Text>
          <TouchableOpacity
            onPress={() => {
              navigation.reset({
                index: 0,
                routes: [{ name: 'LoginScreen' }],
              });
            }}
          >
            <Text style={styles.footerLink}>Login here</Text>
          </TouchableOpacity>
        </View>

      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FCE4EC',
  },
  inputContainer: {
    paddingHorizontal: 24,
    paddingVertical: 24,
  },
  inputTitle: {
    fontSize: 24,
    lineHeight: 40,
    fontWeight: 'bold',
    marginBottom: 14,
  },
  input: {
    fontSize: 16,
    height: 40,
    backgroundColor: '#fff',
    borderColor: '#ddd',
    borderWidth: 1,
    paddingHorizontal: 14,
    marginBottom: 10,
  },
  footerText: {
    fontSize: 16,
    lineHeight: 32,
  },
  footerLink: {
    fontSize: 16,
    lineHeight: 32,
    color: '#D81B60',
  },
});
