import React from 'react';
import {
  View, TextInput, StyleSheet,
} from 'react-native';

import CircleButton from '../components/CircleButton';
import KeyboardSafeView from '../components/KeybordSafeView';

// eslint-disable-next-line react/function-component-definition
export default function MemoCreateScreen(props) {
  const { navigation } = props;
  return (
    <KeyboardSafeView style={styles.container}>

      <View style={styles.inputContainer}>
        <TextInput value="" multiline style={styles.input} />
      </View>

      <CircleButton
        name="check"
        onPress={() => { navigation.goBack(); }}
      />
    </KeyboardSafeView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputContainer: {
    paddingVertical: 30,
    paddingHorizontal: 24,
    flex: 1,
  },
  input: {
    fontSize: 20,
    lineHeight: 24,
    flex: 1,
    textAlignVertical: 'top',
  },
});
