import React, { useState } from 'react';
import {
  View, Text, TextInput, StyleSheet,
} from 'react-native';

// eslint-disable-next-line react/function-component-definition
export default function TestScreen() {
  const [email, setEmail] = useState('');

  return (
    <View style={styles.container}>

      <View style={styles.inputContainer}>
        <Text style={styles.inputTitle}>Input</Text>
        <TextInput
          value={email}
          style={styles.input}
          onChangeText={(text) => { setEmail(text); }}
        />

      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FCE4EC',
  },
  inputContainer: {
    paddingHorizontal: 24,
    paddingVertical: 24,
  },
  inputTitle: {
    fontSize: 24,
    lineHeight: 40,
    fontWeight: 'bold',
    marginBottom: 14,
  },
  input: {
    fontSize: 16,
    height: 40,
    backgroundColor: '#fff',
    borderColor: '#ddd',
    borderWidth: 1,
    paddingHorizontal: 14,
    marginBottom: 10,
  },

});
