import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

// eslint-disable-next-line react/function-component-definition
export default function AppBar() {
  return (
    <View style={styles.appbar}>
      <View style={styles.appbarInner}>
        <Text style={styles.appbarTitle}>MEMO</Text>
        <Text style={styles.appbarLogout}>ログアウト</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  appbar: {
    width: '100%',
    height: 110,
    backgroundColor: '#C51162',
    justifyContent: 'flex-end',
  },
  appbarInner: {
    alignItems: 'center',
  },
  appbarTitle: {
    color: '#fff',
    fontSize: 24,
    lineHeight: 32,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  appbarLogout: {
    position: 'absolute',
    right: 20,
    bottom: 12,
    color: 'rgba(255,255,255,0.7)',
  },
});
