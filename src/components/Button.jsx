import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';
// eslint-disable-next-line import/no-extraneous-dependencies
import { string, func } from 'prop-types';

// eslint-disable-next-line react/function-component-definition
export default function Button(props) {
  const { children, onPress } = props;
  return (
    <TouchableOpacity style={styles.bottonContainer} onPress={onPress}>
      <Text style={styles.bottonLabel}>{children}</Text>
    </TouchableOpacity>
  );
}

Button.propTypes = {
  children: string.isRequired,
  onPress: func,
};

Button.defaultProps = {
  onPress: null,
};

const styles = StyleSheet.create({
  bottonContainer: {
    backgroundColor: '#D81B60',
    borderRadius: 4,
    alignSelf: 'flex-start',
    marginBottom: 24,
  },
  bottonLabel: {
    fontSize: 16,
    lineHeight: 32,
    fontWeight: 'bold',
    paddingVertical: 6,
    paddingHorizontal: 14,
    color: '#fff',
  },
});
