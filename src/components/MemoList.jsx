import React from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { Feather } from '@expo/vector-icons';

// eslint-disable-next-line react/function-component-definition
export default function MemoList() {
  /*
    MemoList.jsxは ナビゲーションに登録されていないため、
    propsでnavigationを受け取ることができない。useNavigationを使う
  */
  const navigation = useNavigation();
  return (
    <View>

      <TouchableOpacity
        style={styles.memoItem}
        onPress={() => { navigation.navigate('MemoDetailScreen'); }}
      >
        <View>
          <Text style={styles.memoItemTitle}>買い物</Text>
          <Text style={styles.memoItemDate}>2020年12月22日 10:00</Text>
        </View>
        <TouchableOpacity
          style={styles.memoDelete}
        >
          <Feather name="x" size={20} color="grey" />
        </TouchableOpacity>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.memoItem}
        onPress={() => { navigation.navigate('MemoDetailScreen'); }}
      >
        <View>
          <Text style={styles.memoItemTitle}>買い物2</Text>
          <Text style={styles.memoItemDate}>2020年12月22日 10:00</Text>
        </View>
        <TouchableOpacity
          style={styles.memoDelete}
        >
          <Feather name="x" size={20} color="grey" />
        </TouchableOpacity>
      </TouchableOpacity>

    </View>
  );
}

const styles = StyleSheet.create({
  memoItem: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 16,
    paddingHorizontal: 19,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'rgba(0,0,0,0.20)',
  },
  memoItemTitle: {
    fontSize: 16,
    lineHeight: 32,
  },
  memoItemDate: {
    fontSize: 12,
    lineHeight: 16,
    color: '#616161',
  },
  memoDelete: {
    padding: 14,
  },
});
