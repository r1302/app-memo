import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { CardStyleInterpolators, createStackNavigator } from '@react-navigation/stack';
import firebase from 'firebase';

import MemoListScreen from './src/screens/MemoListScreen';
import MemoDetailScreen from './src/screens/MemoDetailScreen';
import MemoEditScreen from './src/screens/MemoEditScreen';
import MemoCreateScreen from './src/screens/MemoCreateScreen';
import LoginScreen from './src/screens/LoginScreen';
import SignUpScreen from './src/screens/SignUpScreen';
import TestScreen from './src/screens/TestScreen';

import { firebaseConfig } from './env';

const Stack = createStackNavigator();

if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
}

// eslint-disable-next-line react/function-component-definition
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="LoginScreen"
        screenOptions={{
          headerStyle: { backgroundColor: '#C51162' },
          headerTitleStyle: { color: '#fff' },
          headerTitle: 'Memo',
          headerTintColor: '#fff',
          headerBackTitle: 'Back',
          /* 画面遷移のアニメーションを指定することができる */
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          /* 画面をスワイプすると戻ることができる設定 Android用 iphoneはデフォルトでできる */
          gestureEnavled: true,
          gestureDirection: 'horizontal',
        }}
      >
        <Stack.Screen name="TestScreen" component={TestScreen} />
        <Stack.Screen name="MemoListScreen" component={MemoListScreen} />
        <Stack.Screen name="MemoDetailScreen" component={MemoDetailScreen} />
        <Stack.Screen name="MemoEditScreen" component={MemoEditScreen} />
        <Stack.Screen name="MemoCreateScreen" component={MemoCreateScreen} />
        <Stack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{
            /* 画面遷移のアニメーションを指定することができる */
            cardStyleInterpolator: CardStyleInterpolators.forFadeFromBottomAndroid,
          }}
        />
        <Stack.Screen
          name="SignUpScreen"
          component={SignUpScreen}
          options={{
            /* 画面遷移のアニメーションを指定することができる */
            cardStyleInterpolator: CardStyleInterpolators.forFadeFromBottomAndroid,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
